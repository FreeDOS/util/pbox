REM
REM  ThrowMsg - throw a message in a nice box to the screen
REM    Usage: ThrowMsg(MessageContent AS STRING, SleepTime AS INTEGER = 0)
REM    SleepTime is defined in ms, and is optional (use -1 to wait for a keypress)
REM

#INCLUDE ONCE "wordwrap.bi"

SUB ThrowMsg(MessageContent AS STRING, SleepTime AS INTEGER = 0)
  DIM MsgArray(1 TO 15) AS STRING
  DIM WorkString AS STRING = MessageContent
  DIM AS INTEGER MsgLines = 0, MsgWidth, x, y, StartingLine, StartingColumn, MaxMsgWidth
  IF ScreenWidth \ 2 >= 20 THEN
      MsgWidth = ScreenWidth \ 2
    ELSE
      MsgWidth = 20
  END IF
  WHILE LEN(WorkString) > 0 AND MsgLines < 15
    MsgLines += 1
    MsgArray(MsgLines) = WordWrap(WorkString, MsgWidth)
    IF LEN(MsgArray(MsgLines)) > MaxMsgWidth THEN MaxMsgWidth = LEN(MsgArray(MsgLines))
  WEND
  MsgWidth = MaxMsgWidth
  LOCATE ,,0    ' Turn off the blinking cursor
  StartingLine = (ScreenHeight - MsgLines) \ 2
  StartingColumn = (ScreenWidth - MsgWidth) \ 2 - 1
  COLOR 0, 4
  LOCATE StartingLine - 1, StartingColumn : PRINT GraphTable(1); RepeatPrint(GraphTable(2), MsgWidth + 2); GraphTable(3);
  FOR x = StartingLine TO StartingLine + MsgLines - 1                                         ' Draw borders
    LOCATE x, StartingColumn: PRINT GraphTable(4); SPACE(MsgWidth + 2); GraphTable(4);              ' of the message
  NEXT x                                                                                      ' window.
  LOCATE StartingLine + MsgLines, StartingColumn : PRINT GraphTable(5); RepeatPrint(GraphTable(2), MsgWidth + 2); GraphTable(6);
  COLOR 7, 4
  y = 0                                                              '
  FOR x = StartingLine TO StartingLine + MsgLines - 1                ' Fill in the message window
    y += 1                                                           ' with the message content.
    LOCATE x, StartingColumn + 2: PRINT MsgArray(y);                 '
  NEXT x                                                             '
  IF SleepTime > 0 THEN SLEEP SleepTime, 1  ' If a sleeptime has been specified, go sleep for a while
  IF SleepTime = -1 THEN SLEEP              ' For sleeptime = -1, wait for a keypress
  FlushKeyb()                               ' Flush the keyboard buffer
END SUB
